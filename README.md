## Requirements

You need to get Docker (and Docker Compose) and Git on your computer/server.

## Installation

### Clone project

```bash
git clone https://gitlab.com/boilerplates25/traefik-with-ssl.git
```

### Create your Certs folder

```bash
mkdir configuration/my_cert_folder
```

and put your certs in (.key .cer ...)

### Edit configuration of certificates

Only change path or names of cert in certificates.yml

### Change routes of whoami

Only edit labels in [docker-compose.yml](docker-compose.yml)

```yml
labels:
  - "traefik.http.routers.whoami.rule=Host(`whoami.your_domain.com`)"
```

and

```yml
labels:
  - "traefik.http.routers.whoami_ssl.rule=Host(`whoamissl.your_domain.com`)"
```

### Start Container

```bash
docker compose up --build -d
// OR
docker-compose up --build -d
```

### Test

Now you can go on [whoami.your_domain.com](whoami.your_domain.com) or [whoamissl.your_domain.com](whoamissl.your_domain.com) to check if they are online :)
